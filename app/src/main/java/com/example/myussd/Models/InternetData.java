package com.example.myussd.Models;

public class InternetData {
    private String nomi;
    private String narxi;
    private String hajmi;
    private String muddati;
    private String ussd;

    public InternetData(String nomi, String narxi, String hajmi, String muddati, String ussd) {
        this.nomi = nomi;
        this.narxi = narxi;
        this.hajmi = hajmi;
        this.muddati = muddati;
        this.ussd = ussd;
    }

    public String getNomi() {
        return nomi;
    }

    public String getNarxi() {
        return narxi;
    }

    public String getHajmi() {
        return hajmi;
    }

    public String getMuddati() {
        return muddati;
    }

    public String getUssd() {
        return ussd;
    }
}
