package com.example.myussd.Models;

public class ServiceData {
    private String title;
    private String img;

    public ServiceData(String title, String img) {
        this.title = title;
        this.img = img;
    }

    public String getTitle() {
        return title;
    }

    public String getImg() {
        return img;
    }
}
