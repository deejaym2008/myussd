package com.example.myussd.Models;

public class CompanyData{
    private int id;
    private String name;
    private String img;
    private String textUz;


    public CompanyData(int id, String name, String img, String textUz){
        this.name = name;
        this.id = id;
        this.img = img;
        this.textUz = textUz;
    }

    public int getId() {
        return id;
    }

    public String getTextUz() {
        return textUz;
    }

    public String getImg() {
        return img;
    }

    public String getName() {
        return name;
    }

}
