package com.example.myussd.Models;

public class UssdData {
    private String ussdCode;
    private String title;

    public UssdData(String ussdCode, String title) {
        this.ussdCode = ussdCode;
        this.title = title;
    }

    public String getUssdCode() {
        return ussdCode;
    }

    public String getTitle() {
        return title;
    }
}
