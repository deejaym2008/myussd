package com.example.myussd.Models;

public class ColorId {
    int colorId;

    public ColorId(int colorId) {
        this.colorId = colorId;
    }

    public int getColorId() {
        return colorId;
    }
}
