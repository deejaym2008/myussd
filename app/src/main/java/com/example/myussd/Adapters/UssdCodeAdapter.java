package com.example.myussd.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myussd.Models.UssdData;
import com.example.myussd.R;

import java.util.ArrayList;

public class UssdCodeAdapter extends RecyclerView.Adapter<UssdCodeAdapter.ViewHolder> {
    private ArrayList<UssdData> data;

    public UssdCodeAdapter(ArrayList<UssdData> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_ussd,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UssdData data = this.data.get(position);
        holder.bindData(data);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView ussdCode;
        TextView ussdTitle;
        ImageView call;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ussdCode = itemView.findViewById(R.id.ussd_code);
            ussdTitle = itemView.findViewById(R.id.ussd_title);
            call = itemView.findViewById(R.id.ussd_call);
            call.setOnClickListener(this::onCallClicked);
        }

        private void onCallClicked(View view) {
            Toast.makeText(view.getContext(), "Calling: " + data.get(getAdapterPosition()).getUssdCode(), Toast.LENGTH_LONG).show();
        }

        public void bindData(UssdData data){
            ussdCode.setText(data.getUssdCode());
            ussdTitle.setText(data.getTitle());
        }
    }
}
