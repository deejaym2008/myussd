package com.example.myussd.Adapters;

import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myussd.MainActivity;
import com.example.myussd.Models.InternetData;
import com.example.myussd.R;

import java.util.ArrayList;

public class InternetAdapter extends RecyclerView.Adapter<InternetAdapter.ViewHolder> {
    private ArrayList<InternetData> data;

    public InternetAdapter(ArrayList<InternetData> data) {
        this.data = data;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_internet,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        InternetData data = this.data.get(position);
        holder.bindData(data);
        Resources resources = holder.itemView.getResources();
        int colorId = 0;
        switch(MainActivity.comId){
            case 0:
                colorId = resources.getColor(R.color.colorUmsPrimery);
                break;
            case 1:
                colorId = resources.getColor(R.color.colorUzmobilePrimery);
                break;
            case 2:
                colorId = resources.getColor(R.color.colorBeelinePrimery);
                break;
            case 3:
                colorId = resources.getColor(R.color.colorUcellPrimery);
                break;
            case 4:
                colorId = resources.getColor(R.color.colorPerfectumPrimery);
                break;
        }
        holder.fon.setCardBackgroundColor(colorId);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView nomi;
        TextView narxi;
        TextView hajmi;
        TextView muddati;
        CardView fon;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nomi = itemView.findViewById(R.id.internet_nomi);
            narxi = itemView.findViewById(R.id.internet_narx);
            hajmi = itemView.findViewById(R.id.internet_hajm);
            muddati = itemView.findViewById(R.id.internet_muddat);
            fon = itemView.findViewById(R.id.internet_nomi_fon);
        }
        public void bindData(InternetData data){
            nomi.setText(data.getNomi());
            narxi.setText(data.getNarxi());
            hajmi.setText(data.getHajmi());
            muddati.setText(data.getMuddati());
        }
    }
}
