package com.example.myussd.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.myussd.Models.InternetData;
import com.example.myussd.R;

import java.util.ArrayList;
import java.util.HashMap;

public class InternetPageAdapter extends RecyclerView.Adapter<InternetPageAdapter.ViewHolder> {
    private Context context;
    private HashMap<Integer, ArrayList<InternetData>> hashMap;
    private ArrayList<InternetData> data;
    private InternetAdapter adapter;

    public InternetPageAdapter(Context context, HashMap<Integer, ArrayList<InternetData>> hashMap) {
        this.context = context;
        this.hashMap = hashMap;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.fragment_internet, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        RecyclerView list = holder.list;
        data = hashMap.get(position);
        adapter = new InternetAdapter(data);
        list.setLayoutManager(new LinearLayoutManager(context));
        list.setAdapter(adapter);
    }

    @Override
    public int getItemCount() {
        return hashMap.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerView list;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            list = itemView.findViewById(R.id.internet_list);
        }
    }
}
