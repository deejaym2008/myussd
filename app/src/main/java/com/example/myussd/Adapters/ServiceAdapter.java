package com.example.myussd.Adapters;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myussd.MainActivity;
import com.example.myussd.Models.ColorId;
import com.example.myussd.Models.ServiceData;
import com.example.myussd.R;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.ViewHolder> {

    private Context context;
    private ArrayList<ServiceData> data;
    private OnItemClickListener onItemClickListener;

    public ServiceAdapter(ArrayList<ServiceData> data) {

        this.data = data;
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public ServiceAdapter(ArrayList<ServiceData> data, Context context) {
        this.data = data;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_service, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Resources resources = holder.itemView.getResources();
        ServiceData data = this.data.get(position);

        int imgId = resources.getIdentifier(data.getImg(), "drawable", holder.itemView.getContext().getPackageName());

        //HiveDrawable drawable = new HiveDrawable(HiveLayoutManager.VERTICAL);
        //holder.image.setImageDrawable(drawable);
        //holder.image.setBackground(drawable);
        //holder.image.setPadding(10,10,10,10);
        holder.image.setImageResource(imgId);
        holder.title.setText(data.getTitle());
        int drawable = 0;
        switch (MainActivity.comId) {
            case 0:
                drawable = resources.getIdentifier("service_background", "drawable", holder.itemView.getContext().getPackageName());
                context.setTheme(R.style.AppTheme);
                break;
            case 1:
                drawable = resources.getIdentifier("service_background_uzmobile", "drawable", holder.itemView.getContext().getPackageName());
                context.setTheme(R.style.AppThemeUzm);
                break;
            case 2:
                drawable = resources.getIdentifier("service_background_beeline", "drawable", holder.itemView.getContext().getPackageName());
                context.setTheme(R.style.AppThemeBeeline);
                break;
            case 3:
                drawable = resources.getIdentifier("service_background_ucell", "drawable", holder.itemView.getContext().getPackageName());
                context.setTheme(R.style.AppThemeUcell);
                break;
            case 4:
                drawable = resources.getIdentifier("service_background_perfectum", "drawable", holder.itemView.getContext().getPackageName());
                context.setTheme(R.style.AppThemePerfectum);
                break;
        }

        holder.group.setBackgroundResource(drawable);
        EventBus.getDefault().post(new ColorId(MainActivity.comId));
        setAnimation(holder.itemView, position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onItemClickListener != null){
                    onItemClickListener.OnItemClicked(data,position);
                }
            }
        });
    }

    private void setAnimation(View itemView, int i) {

        boolean isNotFirstItem = i == -1;
        i++;
        itemView.setAlpha(0.f);
        AnimatorSet animatorSet = new AnimatorSet();
        ObjectAnimator animator = ObjectAnimator.ofFloat(itemView, "alpha", 0.f, 0.5f, 1.0f);
        ObjectAnimator.ofFloat(itemView, "alpha", 0.f).start();
        animator.setStartDelay(isNotFirstItem ? 500 / 2 : (i * 500 / 3));
        animator.setDuration(500);
        animatorSet.play(animator);
        animator.start();
    }

    @Override
    public int getItemCount() {

        return data == null ? 0 : data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ViewGroup group;
        ImageView image;
        TextView title;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            group = itemView.findViewById(R.id.group);
            image = itemView.findViewById(R.id.service_img);
            title = itemView.findViewById(R.id.service_title);
        }
    }

    public interface OnItemClickListener {
        void OnItemClicked(ServiceData data, int position);
    }
}
