package com.example.myussd.Adapters;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.example.myussd.Fragments.UssdFragment;
import com.example.myussd.Models.UssdData;

import java.util.ArrayList;
import java.util.HashMap;

public class UssdPagerAdapter extends FragmentPagerAdapter {
    private HashMap<Integer, ArrayList> data;
    private ArrayList<UssdData> ussdDataArrayList;

    public UssdPagerAdapter(@NonNull FragmentManager fm, HashMap<Integer, ArrayList> data) {
        super(fm);
        this.data = data;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        ussdDataArrayList = data.get(position);
        return UssdFragment.getInstance(ussdDataArrayList);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch (position){
            case 0:
                return "UMs";
            case 1:
                return "UzMobile";
            case 2:
                return "Beeline";
            case 3:
                return "Ucell";
            case 4:
                return "Perfectum";
        }
        return null;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }
}
