package com.example.myussd.ui.home;

import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.myussd.Fragments.MainFragment;
import com.example.myussd.Models.ColorId;
import com.example.myussd.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import static com.example.myussd.MainActivity.comId;

public class HomeFragment extends Fragment {
    private FragmentManager manager;
    private FragmentTransaction transaction;

    private HomeViewModel homeViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        manager = getActivity().getSupportFragmentManager();
        transaction = manager.beginTransaction();

        MainFragment fragment = new MainFragment(getContext());
        transaction.replace(R.id.group,fragment);
        transaction.commit();
        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    @Subscribe
    public void changeAppColor(ColorId colorId){
        int idColor;
        switch (comId){
            case 0:
                //getTheme().applyStyle(R.style.AppTheme,true);
                getActivity().getApplication().setTheme(R.style.AppTheme);
                idColor = getResources().getColor(R.color.colorUmsPrimery);
                break;
            case 1:
                //getTheme().applyStyle(R.style.AppThemeUzm,true);
                getActivity().getApplication().setTheme(R.style.AppThemeUzm);
                idColor = getResources().getColor(R.color.colorUzmobilePrimery);
                break;
            case 2:
                //getTheme().applyStyle(R.style.AppThemeBeeline,true);
                getActivity().getApplication().setTheme(R.style.AppThemeBeeline);
                idColor = getResources().getColor(R.color.colorBeelinePrimery);
                break;
            case 3:
                //getTheme().applyStyle(R.style.AppThemeUcell,true);
                getActivity().getApplication().setTheme(R.style.AppThemeUcell);
                idColor = getResources().getColor(R.color.colorUcellPrimery);
                break;
            case 4:
//                getTheme().applyStyle(R.style.AppThemePerfectum,true);
                getActivity().getApplication().setTheme(R.style.AppThemePerfectum);
                idColor = getResources().getColor(R.color.colorPerfectumPrimery);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + comId);
        }
        //toolbar.setBackgroundColor(idColor);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getActivity().getWindow().setNavigationBarColor(idColor);
            getActivity().getWindow().setStatusBarColor(idColor);
        }


    }
}