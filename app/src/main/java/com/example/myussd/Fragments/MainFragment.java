package com.example.myussd.Fragments;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myussd.Adapters.CompanyAdapter;
import com.example.myussd.Adapters.ServiceAdapter;
import com.example.myussd.MainActivity;
import com.example.myussd.Models.CompanyData;
import com.example.myussd.Models.CompanyId;
import com.example.myussd.Models.ServiceData;
import com.example.myussd.Models.UssdData;
import com.example.myussd.R;
import com.example.myussd.ui.home.HomeFragment;
import com.romainpiel.shimmer.Shimmer;
import com.romainpiel.shimmer.ShimmerTextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;

import cdflynn.android.library.turn.TurnLayoutManager;

public class MainFragment extends Fragment implements ServiceAdapter.OnItemClickListener {
    private Context context;

    private CardView logoCard;
    private Shimmer shimmer;
    private ShimmerTextView shimmerTextView;
    private ImageView logo;
    private ImageView bottom_lines;
    private RecyclerView companyList;
    private RecyclerView serviceList;

    private ArrayList<ServiceData> serviceData;
    private ArrayList<CompanyData> companyData;
    private ArrayList<UssdData> ussdData;
    private HashMap<Integer, ArrayList<UssdData>> ussdMap;

    private CompanyAdapter companyAdapter;
    private ServiceAdapter serviceAdapter;

    private FragmentManager manager;
    private FragmentTransaction transaction;

    private int compId =0;

    public MainFragment(Context context) {
        this.context = context;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Toast.makeText(context, "Fragment CREATED!!!", Toast.LENGTH_LONG).show();
        loadData();
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        Bundle bundle = getArguments();
        findViews(view);

        //bottom_lines.setTranslationY(-bottom_lines.getHeight()/2);

        companyAdapter = new CompanyAdapter(companyData);
        serviceAdapter = new ServiceAdapter(serviceData, context);

        serviceList.setLayoutManager(new GridLayoutManager(context,2));
        TurnLayoutManager layoutManager = new TurnLayoutManager(context, TurnLayoutManager.Gravity.END, TurnLayoutManager.Orientation.HORIZONTAL,1000,60,true);
        companyList.setLayoutManager(layoutManager);
        layoutManager.scrollToPosition(companyData.size()/2);

        serviceList.setAdapter(serviceAdapter);
        CompanyData data = null;
        for(int i=0; i<companyData.size(); i++){
            if (companyData.get(i).getId() == MainActivity.comId){
                data = companyData.get(i);
            }
        }
        onChangeCompany(data);
        companyList.setAdapter(companyAdapter);

        serviceAdapter.setOnItemClickListener(this);
        companyAdapter.setOnItemClickListener(new CompanyAdapter.OnItemClickListener() {
            @Override
            public void onItemClicke(CompanyData data, int position) {
//                MainActivity.comId = data.getId();
                EventBus.getDefault().post(new CompanyId(data.getId()));
                compId = data.getId();
                onChangeCompany(data);
            }
        });

        return view;
    }

    private void onChangeCompany(CompanyData data) {
        Toast.makeText(getContext(), data.getName(), Toast.LENGTH_SHORT).show();

        serviceList.setAdapter(serviceAdapter);

        Resources resources = getResources();
        int imgId = resources.getIdentifier(data.getImg(),"drawable",getContext().getPackageName());
        logo.setImageResource(imgId);

        shimmerTextView.setText(data.getTextUz());
        int colorId;
        switch (data.getId()){
            case 0:
                colorId = resources.getColor(R.color.colorUmsPrimery);
                break;
            case 1:
                colorId = resources.getColor(R.color.colorUzmobilePrimery);
                break;
            case 2:
                colorId = resources.getColor(R.color.colorBeelinePrimery);
                break;
            case 3:
                colorId = resources.getColor(R.color.colorUcellPrimery);
                break;
            case 4:
                colorId = resources.getColor(R.color.colorPerfectumPrimery);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + data.getId());
        }
        Drawable drawable = resources.getDrawable(R.drawable.ic_btn_back);
        drawable.setColorFilter(colorId, PorterDuff.Mode.SRC_IN);
        bottom_lines.setImageDrawable(drawable);
        shimmerTextView.setTextColor(colorId);
        logoCard.setCardBackgroundColor(resources.getColor(R.color.colorWite));
    }

    @Override
    public void onResume() {
        shimmer = new Shimmer();
        shimmer.start(shimmerTextView);
        super.onResume();
    }

    private void findViews(View view) {
        logoCard = view.findViewById(R.id.logo_card);
        companyList = view.findViewById(R.id.list);
        serviceList = view.findViewById(R.id.service_list);
        shimmerTextView = view.findViewById(R.id.logo_text);
        logo = view.findViewById(R.id.logo);
        bottom_lines = view.findViewById(R.id.bottom_back);
    }

    private void loadData() {
        ussdMap = new HashMap<>();

        serviceData = new ArrayList<>();
        companyData = new ArrayList<>();

        serviceData.add(new ServiceData("USSD kodlar", "ic_ussd"));
        serviceData.add(new ServiceData("Internet paketlar", "ic_internet"));
        serviceData.add(new ServiceData("Tarif rejalar", "ic_tarifreja"));
        serviceData.add(new ServiceData("Daqiqa", "ic_daqiqa"));
        serviceData.add(new ServiceData("SMS paketlar", "ic_sms"));
        serviceData.add(new ServiceData("Xizmatlar", "ic_xizmat"));

        for (int i = 0; i < 100; i++) {
            companyData.add(new CompanyData(0, "UMS", "ums", "Talablarga mos aloqa operatori"));
            companyData.add(new CompanyData(1, "UzMobile", "uzmobile", "Milliy operator"));
            companyData.add(new CompanyData(2, "Beeline", "beeline", "Hayotning yorqin tarafida bo'l!"));
            companyData.add(new CompanyData(3, "Ucell", "ucell", "Hayotni yaxshilik sari o'zgartirib!"));
            companyData.add(new CompanyData(4, "Perfectum", "perfectum", "Perfectum! Bugun! Abadiy!"));
        }

        // USSD codlarni yuklash
        // UMS ussd codlari
        ussdData = new ArrayList<>();
            ussdData.add(new UssdData("*100#", "Balansni ko'rish"));
            ussdData.add(new UssdData("150#", "Shaxsiy raqamni ko'rish"));
            ussdData.add(new UssdData("102#", "Sotib olingan internet paket MB ni ko'rish"));
            ussdData.add(new UssdData("103#", "Sotib olingan daqiqani ko'rish"));
            ussdData.add(new UssdData("104#", "Sotib olingan SMS paketni ko'rish"));
        ussdMap.put(0,ussdData);

        // UZMOBILE ussd codlari
        ussdData = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            ussdData.add(new UssdData("*#002#", "Proverka status telefona"));
            ussdData.add(new UssdData("*#020#", "Proverka status telefona"));
            ussdData.add(new UssdData("*#202#", "Proverka status telefona"));
        }
        ussdMap.put(1,ussdData);

        // BEELINE ussd codlari
        ussdData = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            ussdData.add(new UssdData("*#003#", "Proverka status telefona"));
            ussdData.add(new UssdData("*#030#", "Proverka status telefona"));
            ussdData.add(new UssdData("*#303#", "Proverka status telefona"));
        }
        ussdMap.put(2,ussdData);

        // UCELL ussd codlari
        ussdData = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            ussdData.add(new UssdData("*#004#", "Proverka status telefona"));
            ussdData.add(new UssdData("*#040#", "Proverka status telefona"));
            ussdData.add(new UssdData("*#404#", "Proverka status telefona"));
        }
        ussdMap.put(3,ussdData);

        // PERFECTUM ussd codlari
        ussdData = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            ussdData.add(new UssdData("*#005#", "Proverka status telefona"));
            ussdData.add(new UssdData("*#050#", "Proverka status telefona"));
            ussdData.add(new UssdData("*#505#", "Proverka status telefona"));
        }
        ussdMap.put(4,ussdData);

    }

    @Override
    public void OnItemClicked(ServiceData data, int position) {

        manager = getActivity().getSupportFragmentManager();
        transaction = manager.beginTransaction();
        switch (position){
            case 0: case 2: case 5:
                //UssdPageFragment fragment = new UssdPageFragment();
                UssdFragment fragment = UssdFragment.getInstance(ussdMap.get(compId));
                transaction.replace(R.id.group,fragment);
                break;
            case 1: case 3: case 4:
                InternetPagerFragment internetPageFragment = InternetPagerFragment.getInstance(compId);
                transaction.replace(R.id.group,internetPageFragment);
                break;
        }

        transaction.addToBackStack("MAIN");
        transaction.commit();
    }

//    public static MainFragment getInstance(CompanyData data){
//        MainFragment fragment = new MainFragment();
//        Bundle bundle = new Bundle();
//        fragment.setArguments(bundle);
//        return fragment;
//    }
}
