package com.example.myussd.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myussd.Adapters.InternetAdapter;
import com.example.myussd.Models.InternetData;
import com.example.myussd.R;

import java.util.ArrayList;

public class InternetFragment extends Fragment {
    private RecyclerView list;
    private InternetAdapter adapter;
    private ArrayList<InternetData> data;

    public InternetFragment() {
        super();
    }

    public InternetFragment(ArrayList<InternetData> data) {
        super();
        this.data = data;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_internet, container, false);
        Bundle bundle = getArguments();
        //data = (ArrayList<InternetData>) bundle.get("DATA");
        list = view.findViewById(R.id.internet_list);
        adapter = new InternetAdapter(data);
        list.setLayoutManager(new LinearLayoutManager(getContext()));
        list.setAdapter(adapter);
        return view;
    }


    public static InternetFragment getInstance(ArrayList<InternetData> data) {
        InternetFragment fragment = new InternetFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("DATA", data);
        fragment.setArguments(bundle);
        return fragment;
    }


}
