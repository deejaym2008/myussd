package com.example.myussd.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.myussd.Adapters.UssdPagerAdapter;
import com.example.myussd.Models.UssdData;
import com.example.myussd.R;

import java.util.ArrayList;
import java.util.HashMap;

public class UssdPageFragment extends Fragment {
    private ViewPager viewPager;
    private UssdPagerAdapter pagerAdapter;
    private HashMap<Integer, ArrayList> hashMap;

    public UssdPageFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ussd_pager, container, false);
        Bundle bundle = getArguments();
        loadData();
        viewPager = view.findViewById(R.id.ussd_pager);
        pagerAdapter = new UssdPagerAdapter(getActivity().getSupportFragmentManager(), hashMap);
        viewPager.setAdapter(pagerAdapter);
        //viewPager.setCurrentItem(MainActivity.comId);
        return view;
    }

    private void loadData() {
        hashMap = new HashMap<>();
        ArrayList<UssdData> ussdDataArrayList = new ArrayList<>();

        for (int i = 0; i < 15; i++) {
            ussdDataArrayList.add(new UssdData("*#0000#", "Proverka status telefona"));
        }
        hashMap.put(0, ussdDataArrayList);

        ussdDataArrayList = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            ussdDataArrayList.add(new UssdData("*#1111#", "Proverka status telefona"));
        }
        hashMap.put(1, ussdDataArrayList);

        ussdDataArrayList = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            ussdDataArrayList.add(new UssdData("*#2222#", "Proverka status telefona"));
        }
        hashMap.put(2, ussdDataArrayList);

        ussdDataArrayList = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            ussdDataArrayList.add(new UssdData("*#3333#", "Proverka status telefona"));
        }
        hashMap.put(3, ussdDataArrayList);

        ussdDataArrayList = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            ussdDataArrayList.add(new UssdData("*#4444#", "Proverka status telefona"));
        }
        hashMap.put(4, ussdDataArrayList);
    }
}
