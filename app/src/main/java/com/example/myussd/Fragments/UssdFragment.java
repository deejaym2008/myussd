package com.example.myussd.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.myussd.Adapters.UssdCodeAdapter;
import com.example.myussd.Models.UssdData;
import com.example.myussd.R;

import java.util.ArrayList;

public class UssdFragment extends Fragment {
    private RecyclerView list;
    private UssdCodeAdapter adapter;
    private ArrayList<UssdData> data;

    public UssdFragment(ArrayList<UssdData> data) {
        super();
        this.data = data;
    }

    public UssdFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ussd, container, false);
        //loadData();
        Bundle bundle = getArguments();
        data = (ArrayList<UssdData>) bundle.get("DATA");

        list = view.findViewById(R.id.ussd_list);
        adapter = new UssdCodeAdapter(data);
        list.setLayoutManager(new LinearLayoutManager(view.getContext()));
        list.setAdapter(adapter);


        return view;
    }

    private void loadData() {
        data = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            data.add(new UssdData("*#06#", "IMEI codni aniqlash"));
        }
    }

    public static UssdFragment getInstance(ArrayList<UssdData> ussdDataArrayList) {
        UssdFragment fragment = new UssdFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("DATA", ussdDataArrayList);
        fragment.setArguments(bundle);
        return fragment;
    }
}
