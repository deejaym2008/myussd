package com.example.myussd.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager2.widget.ViewPager2;

import com.example.myussd.Adapters.InternetPageAdapter;
import com.example.myussd.Models.InternetData;
import com.example.myussd.R;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

import java.util.ArrayList;
import java.util.HashMap;

public class InternetPagerFragment extends Fragment {
    private HashMap<Integer, ArrayList<InternetData>> umsData;
    private HashMap<Integer, ArrayList<InternetData>> uzmobileData;
    private HashMap<Integer, ArrayList<InternetData>> ucellData;
    private HashMap<Integer, ArrayList<InternetData>> beelineData;
    private HashMap<Integer, ArrayList<InternetData>> perfectumData;
    private ViewPager2 pager2;
    private InternetPageAdapter adapter;
    private TextView textView;
    private TabLayout tabLayout;
    private int comdId;

    private InternetPagerFragment() {
        super();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Bundle bundle = getArguments();
        comdId = bundle.getInt("COMPANY_ID");
        View view = inflater.inflate(R.layout.fragment_internet_pager, container, false);
        pager2 = view.findViewById(R.id.internet_pager);
        textView = view.findViewById(R.id.internet_status);
        tabLayout = view.findViewById(R.id.tab_pager);
        loadData();
        adapter = new InternetPageAdapter(getContext(), umsData);
        pager2.setAdapter(adapter);
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(tabLayout, pager2, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText(Integer.toString(position));
            }
        });
        tabLayoutMediator.attach();
        return view;
    }

    private void loadData() {
        umsData = new HashMap<>();

        // Internet paketlar haqidagi ma'lumotlar yuklanishi
        // 30 kunli internet paketlari
        ArrayList<InternetData> data = new ArrayList<>();
        data.add(new InternetData("500", "10 000 so'm", "500 mb", "30 kun", "*147*10072*13625#"));
        data.add(new InternetData("1 500", "15 000 so'm", "1500 mb", "30 kun", "*147*10073*13625#"));
        data.add(new InternetData("3 000", "24 000 so'm", "3000 mb", "30 kun", "*147*10074*13625#"));
        data.add(new InternetData("5 000", "32 000 so'm", "5000 mb", "30 kun", "*147*10075*13625#"));
        data.add(new InternetData("8 000", "41 000 so'm", "8000 mb", "30 kun", "*147*10076*13625#"));
        data.add(new InternetData("12 000", "50 000 so'm", "12000 mb", "30 kun", "*147*10077*13625#"));
        data.add(new InternetData("20 000", "65 000 so'm", "20000 mb", "30 kun", "*147*10078*13625#"));
        data.add(new InternetData("30 000", "75 000 so'm", "30 000 mb", "30 kun", "*147*10079*13625#"));
        data.add(new InternetData("50 000", "85 000 so'm", "50 000 mb", "30 kun", "*147*10080*13625#"));
        data.add(new InternetData("75 000", "110 000 so'm", "75 000 mb", "30 kun", "*147*10150*13625#"));
        umsData.put(0, data);

        // UZMOBILE internet paketlari
        data = new ArrayList<>();
        for (int i = 0; i < 15; i++)
            data.add(new InternetData("500", "20 000 so'm", "500 mb", "30 kun","*147*10150*13625#"));
        data.add(new InternetData("700", "22 000 so'm", "700 mb", "30 kun","*147*10150*13625#"));
        umsData.put(1, data);

        // BEELINE internet paketlari
        data = new ArrayList<>();
        for (int i = 0; i < 15; i++)
            data.add(new InternetData("1000", "25 000 so'm", "1000 mb", "30 kun","*147*10150*13625#"));
        data.add(new InternetData("1500", "30 000 so'm", "1500 mb", "30 kun","*147*10150*13625#"));
        umsData.put(2, data);

        // UCELL internet paketlari
        data = new ArrayList<>();
        for (int i = 0; i < 15; i++)
            data.add(new InternetData("2000", "32 000 so'm", "2000 mb", "30 kun","*147*10150*13625#"));
        data.add(new InternetData("3000", "35 000 so'm", "3000 mb", "30 kun","*147*10150*13625#"));
        umsData.put(3, data);
    }

    public static InternetPagerFragment getInstance(int compId) {
        InternetPagerFragment fragment = new InternetPagerFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("COMPANY_ID", compId);
        fragment.setArguments(bundle);
        return fragment;
    }
}
